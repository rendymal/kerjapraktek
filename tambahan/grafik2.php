<?php
session_start();
include "../config/koneksi.php";
include "../config/function.php";
if( !isset($_SESSION["login"]))
{
    header("Location: login.php");
    exit;
}
$air      = mysqli_query($koneksi, "SELECT air FROM tbl_air ORDER BY id ASC");
$waktu = mysqli_query($koneksi, "SELECT waktu FROM tbl_air ORDER BY id ASC");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Grafik Pengukuran Ketinggian Air</title>
    <script src="../config/Chart.js"></script>
    <style type="text/css">
            .container {
                width: 40%;
                margin: 15px auto;
            }
    </style>
  </head>
  <body>
    <center>
      <h2>Grafik Hasil Pengukuran Ketinggian Air</h2>
      <div style="width: 80%; height: 250px;">
          <canvas id="linechart"></canvas>
      </div>
    </center>
        <script  type="text/javascript">
          var ctx = document.getElementById("linechart").getContext("2d");
          var data = {
                    labels: [<?php while($p = mysqli_fetch_array($waktu)){echo '"'. $p['waktu']. '",';} ?>],
                    datasets: [
                          {
                            label: "Ketinggian Air",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "#29B0D0",
                            borderColor: "#29B0D0",
                            pointHoverBackgroundColor: "#29B0D0",
                            pointHoverBorderColor: "#29B0D0",
                            data: [<?php while($p = mysqli_fetch_array($air)){echo '"'. $p['air'].'",';} ?>]
                          }
                          ]
                  };

          var myBarChart = new Chart(ctx, {
                    type: 'line',
                    data: data,
                    options: {
                    legend: {
                      display: true
                    },
                    barValueSpacing: 20,
                    scales: {
                      yAxes: [{
                          ticks: {
                              min: 0,
                          }
                      }],
                      xAxes: [{
                                  gridLines: {
                                      color: "rgba(0, 0, 0, 0)",
                                  }
                              }]
                      }
                  }
                });
        </script>
  </body>
</html>