<?php
session_start();
include "../config/koneksi.php";
include "../config/function.php";
if( !isset($_SESSION["login"]))
{
    header("Location: login.php");
    exit;
}
if(isset($_POST['bsimpan']))
{
// simpan data baru
// simpan data
    $simpan = mysqli_query($koneksi, "INSERT INTO tbl_kelembaban
    VALUES ('', '$_POST[tanggal]', 
                '$_POST[jam]',
                '$_POST[kelembaban]')
            ");
}
$nilai = mysqli_query($koneksi, "SELECT * FROM tbl_kelembaban WHERE id IN (SELECT MAX(id) FROM tbl_kelembaban)");
$variabel = mysqli_fetch_array($nilai);
$vtanggal = $variabel['tanggal'];
$vjam = $variabel['waktu'];
$vkelembaban = $variabel['kelembaban'];
  ?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="../atur.css">
</head>
<body>

<div class="">
    <div class="cf">
        <input type="checkbox" id="check">
            <div class="sidebar">
                <ul>
                    <li><a href="admin.php">SUHU</a></li>
                    <li><a href="admin1.php">KELEMBABAN</a></li>
                    <li><a href="admin2.php">KETINGGIAN AIR</a></li>
                    <li><a href="suhu.php">Kembali Ke Index</a></li>
                </ul>
            </div>
        <header class="header">
            <div class="logo">
            <span>LABORATORIUM OSEANOGRAFI FISIS DAN SAINS ATMOSFIR</span>
            </div>
            <div class="link">
                <ul>
                    <li><a href="admin.php">SUHU</a></li>
                    <li><a href="admin1.php">KELEMBABAN</a></li>
                    <li><a href="admin2.php">KETINGGIAN AIR</a></li>
                    <li><a href="suhu.php">Kembali Ke Index</a></li>
                </ul>
                <label for="check" class="menu">menu</label>
            </div>
        </header>
    </div>

    <div class="form1">
      <div class="form2">
        Form Data Kelembaban
      </div>
      <div>
        <form method="post" action="">
          <div class="form3">
            <label for="tanggal">Tanggal : </label><br>
            <input type="date" class="form4" id="tanggal" name="tanggal" value="<?=@$vtanggal?>">
            <br>
            <label for="jam">Jam : </label><br>
            <input type="Time" class="form4" id="jam" name="jam" value="<?=@$vjam?>" required>
            <br>
            <label for="kelembaban">Kelembaban : </label><br>
            <input type="float" class="form4" id="kelembaban" name="kelembaban" value="<?=@$vkelembaban?>">
          </div>
          <button type="submit" name="bsimpan" class="form5">Simpan</button>
        </form>
       </div>
    </div>

    <div class="table">
        <?php 
            $tampil = mysqli_query($koneksi, "SELECT * FROM tbl_kelembaban WHERE id IN (SELECT MAX(id) FROM tbl_kelembaban)");
            $data = mysqli_fetch_array($tampil);
        ?>
        <div class="dua">Inderalaya, Sumatera Selatan</div>
        <div class="satu">Kelembaban Udara</div>
        <div class="tiga"><?=$data['kelembaban'] ?> % </div>
        <table>
        <?php 
        $jumlahdataperhalaman = 10;
        $jumlahdata = count(query("SELECT * FROM tbl_kelembaban ORDER BY id DESC"));
        $jumlahhalaman = ceil($jumlahdata/$jumlahdataperhalaman);
        $halamanaktif = (isset($_GET['hal'])) ? $_GET['hal'] : 1;
        $awaldata = ($jumlahdataperhalaman * $halamanaktif) - $jumlahdataperhalaman;
        $show1 = query("SELECT * FROM tbl_kelembaban ORDER BY id DESC LIMIT $awaldata, $jumlahdataperhalaman");
        $i= 1 + $awaldata;
        $jumlahlink = 2;
        if($halamanaktif > $jumlahlink)
        {
            $start_number = $halamanaktif - $jumlahlink;
        }
        else
        {
            $start_number = 1;
        }
        if($halamanaktif < ($jumlahhalaman - $jumlahlink))
        {
            $end_number = $halamanaktif + $jumlahlink;
        }
        else
        {
            $end_number = $jumlahhalaman;
        }
        foreach($show1 as $data1) :
        $seminggu = array('Sunday'=>'Minggu', 'Monday'=>'Senin', 'Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jumat','Saturday'=>'Sabtu');
        $date = $data1['tanggal'];
        $hari= date('l', strtotime($date));
         ?>
        <tr>
            <td><?= $i;?></td>
            <td><?=$seminggu[$hari] ?></td>
            <td><?=$data1['tanggal'] ?></td>
            <td><?=$data1['waktu'] ?></td>
            <td><?=$data1['kelembaban'] ?> %</td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
        </table>
        <!-- navigasi -->
        <center>
            <div class="navigasi">
                <a href="?hal=<?=1 ?>" style="font-weight: bold; color: white; text-decoration: none;"> <<< </a>
            <?php if($halamanaktif > 1): ?>
                <a href="?hal=<?=$halamanaktif-1; ?>" style=" color: white; text-decoration: none;"> previous </a>
            <?php endif; ?>
            <?php for($i=$start_number; $i <= $end_number; $i++) : ?>
                <?php if($i == $halamanaktif) : ?>
                    <a href="?hal=<?=$i;?>" style="font-weight: bold; color: red; text-decoration: none;"><?=$i;?></a>
                <?php else : ?>
                    <a href="?hal=<?=$i;?>" style="color: white; text-decoration: none;"><?=$i;?></a>
                <?php endif; ?>
            <?php endfor; ?>
            <?php if($halamanaktif < $jumlahhalaman): ?>
                <a href="?hal=<?=$halamanaktif+1; ?>" style=" color: white; text-decoration: none;"> next </a>
            <?php endif; ?>
                <a href="?hal=<?=$jumlahhalaman ?>" style="font-weight: bold; color: white; text-decoration: none;"> >>> </a>
            </div>
        </center>
    </div>
    <form method="POST" action='' align="center">
        <input type="button" class="btnsasa" value="Download Data" onclick="document.location.href='../tambahan/unduh1.php'" />
        <input type="button" class="btnsasa" value="Tampilkan Grafik" onclick="document.location.href='../tambahan/grafik1.php'" />
    </form>
    <div>
        <footer><span>Fisika FMIPA UNSRI</span></footer>
    </div>
</div>

</body>
</html>