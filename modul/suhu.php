<?php
session_start();
include "../config/koneksi.php";
include "../config/function.php";
if( !isset($_SESSION["login"]))
{
    header("Location: login.php");
    exit;
}
  ?>
<!DOCTYPE html>
<html>
<head>
    <title>project kp</title>
    <link rel="stylesheet" href="../atur.css">
</head>
<body>

<div class="">
    <div class="cf">
        <input type="checkbox" id="check">
            <div class="sidebar">
                <ul>
                    <li><a href="suhu.php">SUHU</a></li>
                    <li><a href="kelembaban.php">KELEMBABAN</a></li>
                    <li><a href="air.php">KETINGGIAN AIR</a></li>
                    <li><a href="../admin.php">ADMIN</a></li>
                    <li><a href="../logout.php">LOGOUT</a></li>
                </ul>
            </div>
        <header class="header">
            <div class="logo">
            <span>LABORATORIUM OSEANOGRAFI FISIS DAN SAINS ATMOSFIR</span>
            </div>
            <div class="link">
                <ul>
                    <li><a href="suhu.php">SUHU</a></li>
                    <li><a href="kelembaban.php">KELEMBABAN</a></li>
                    <li><a href="air.php">KETINGGIAN AIR</a></li>
                    <li><a href="../admin.php">ADMIN</a></li>
                    <li><a href="../logout.php">LOGOUT</a></li>
                </ul>
                <label for="check" class="menu">menu</label>
            </div>
        </header>
    </div>
    <div class="table">
        <?php 
            $tampil = mysqli_query($koneksi, "SELECT * FROM tbl_suhu WHERE id IN (SELECT MAX(id) FROM tbl_suhu)");
            $data = mysqli_fetch_array($tampil);
        ?>
        <div class="dua">Inderalaya, Sumatera Selatan</div>
        <div class="satu">Temperatur</div>
        <div class="tiga"><?=$data['suhu'] ?>&deg;C </div>
        <table>
        <?php 
        $jumlahdataperhalaman = 20;
        $jumlahdata = count(query("SELECT * FROM tbl_suhu ORDER BY id DESC"));
        $jumlahhalaman = ceil($jumlahdata/$jumlahdataperhalaman);
        $halamanaktif = (isset($_GET['hal'])) ? $_GET['hal'] : 1;
        $awaldata = ($jumlahdataperhalaman * $halamanaktif) - $jumlahdataperhalaman;
        $show = query("SELECT * FROM tbl_suhu ORDER BY id DESC LIMIT $awaldata, $jumlahdataperhalaman");
        $i= 1 + $awaldata;
        $jumlahlink = 2;
        if($halamanaktif > $jumlahlink)
        {
            $start_number = $halamanaktif - $jumlahlink;
        }
        else
        {
            $start_number = 1;
        }
        if($halamanaktif < ($jumlahhalaman - $jumlahlink))
        {
            $end_number = $halamanaktif + $jumlahlink;
        }
        else
        {
            $end_number = $jumlahhalaman;
        }
        foreach($show as $data) :
        $seminggu = array('Sunday'=>'Minggu', 'Monday'=>'Senin', 'Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jumat','Saturday'=>'Sabtu');
        $date = $data['tanggal'];
        $hari= date('l', strtotime($date));
         ?>
        <tr>
            <td><?= $i;?></td>
            <td><?=$seminggu[$hari] ?></td>
            <td><?=$data['tanggal'] ?></td>
            <td><?=$data['waktu'] ?></td>
            <td><?=$data['suhu'] ?>&deg;C</td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
        </table>
        <!-- navigasi -->

        <center>
            <div class="navigasi">
                <a href="?hal=<?=1 ?>" style="font-weight: bold; color: white; text-decoration: none;"> <<< </a>
            <?php if($halamanaktif > 1): ?>
                <a href="?hal=<?=$halamanaktif-1; ?>" style=" color: white; text-decoration: none;"> previous </a>
            <?php endif; ?>
            <?php for($i=$start_number; $i <= $end_number; $i++) : ?>
                <?php if($i == $halamanaktif) : ?>
                    <a href="?hal=<?=$i;?>" style="font-weight: bold; color: red; text-decoration: none;"><?=$i;?></a>
                <?php else : ?>
                    <a href="?hal=<?=$i;?>" style="color: white; text-decoration: none;"><?=$i;?></a>
                <?php endif; ?>
            <?php endfor; ?>
            <?php if($halamanaktif < $jumlahhalaman): ?>
                <a href="?hal=<?=$halamanaktif+1; ?>" style=" color: white; text-decoration: none;"> next </a>
            <?php endif; ?>
                <a href="?hal=<?=$jumlahhalaman ?>" style="font-weight: bold; color: white; text-decoration: none;"> >>> </a>
            </div>
        </center>
    </div>
    <div>
        <footer><span>Fisika FMIPA UNSRI</span></footer>
    </div>
</div>

</body>
</html>